import com.devcamp.s10.jbr220.Circle;
import com.devcamp.s10.jbr220.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle(2.0);
        Circle circle2 = new Circle(3.0, "green");
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, "green");
        Cylinder cylinder4 = new Cylinder(3.5, 1.5, "green");
        System.out.println(cylinder1.toString());
        System.out.println(cylinder2.toString());
        System.out.println(cylinder3.toString());
        System.out.println(cylinder4.toString());
    }
}
